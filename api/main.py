import os
from fastapi import FastAPI
from routers import beverages_routers, accounts_routers, dairies_routers, grains_routers, produces_routers, proteins_routers


app = FastAPI()



app.include_router(accounts_routers.router)
app.include_router(beverages_routers.router)
app.include_router(dairies_routers.router)
app.include_router(grains_routers.router)
app.include_router(produces_routers.router)
app.include_router(proteins_routers.router)
